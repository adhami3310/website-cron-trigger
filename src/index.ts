export interface Env {
	URL: string;
}

export default {
	async scheduled(event: ScheduledEvent, env: Env, ctx: ExecutionContext): Promise<void> {
		let resp = await fetch(env.URL, {
			method: 'POST',
		});
		let wasSuccessful = resp.ok ? 'success' : 'fail';
		console.log(`trigger fired at ${event.cron}: ${wasSuccessful}`);
	},
};
